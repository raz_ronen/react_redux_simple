// we need to connect this action to redux so all reducers will recieve the update.
// action creator:
export function selectBook(book){
   // selectBook is an ActionCreator, it need to return an action,
    // an object wit a type property.
    return {
       type: 'BOOK_SELECTED',
        payload: book
   };
}
