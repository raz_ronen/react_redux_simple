import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
class BookDetail extends Component {
    render(){
        //FOR START - everything null:
        if(!this.props.book){
            return <div>Select a book to get started.</div>;
        }
        return (
            <div>
                <h3>Details for:</h3>
                <div>Title: {this.props.book.title}</div>
                <div>Pages: {this.props.book.pages}</div>
            </div>
        );
    }
}

//connect from redux state to react state. via connect function
function mapStateToProps(state){
    // state.activeBook comes from reducers.index.js
    return {
        book: state.activeBook
    };
}

export default connect(mapStateToProps)(BookDetail);


