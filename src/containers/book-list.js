import React, {Component} from 'react';
import { connect } from 'react-redux';
import { selectBook } from '../actions/index';
import { bindActionCreators } from 'redux';
// bindActionCreators is the part where we take the return value
// from selectBook actually gets flowing to all the different reducers in our application
class BookList extends Component {

    renderList(){
        return this.props.books.map((book) => {
            return (<li
                key={book.title}
                onClick={() => this.props.selectBook(book)}
                className="list-group-item">{book.title}</li>);
        })
    }

    render(){
        return (
            <ul className="list-group col_sm_4">
                {this.renderList()}
            </ul>
        )
    }
}

// state us from redux - App state. and we return the Component(Cotainer) state
// whenever App state change the Container will automatticly rerender.
function mapStateToProps(state){
    // Whatever is returned will show up as props inside of BookList
    return {
      books: state.books
    };
}

//Anything returned from this function will end up as props
// on the BookList container
function mapDispathToProps(dispath){
    // Whenever selectBook is called the result should be passed
    // to all of our reducers.
    // the result will go to dispath and this will pass the result to all the reducers
    // the left selectBook will be the key inside props
    return bindActionCreators({ selectBook: selectBook}, dispath);
}

// Promote BookList from a Component to a container - it needs to know
// about this new dispath method, selectBook. Make it available
// as a prop.
export default connect(mapStateToProps, mapDispathToProps)(BookList);
