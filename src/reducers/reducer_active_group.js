//reducer only calls when action occur, so action will allways not be null.
//the state is the only the state that this reducer is responsible foro.
// -meaning the key from combineReducers in index.js
// ** ES6 syntax - if state is undefined, it sets it to null.
//    redux doesn't allow us returning undefined.
export default function(state = null, action){
    switch(action.type) {
        case 'BOOK_SELECTED':
            return action.payload;
        default:
            return state;
    }
}